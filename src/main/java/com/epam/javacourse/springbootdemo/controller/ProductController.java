package com.epam.javacourse.springbootdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.javacourse.springbootdemo.repository.ProductRepository;



@Controller
public class ProductController {
	
	@Autowired
	private ProductRepository repository;
	
	@RequestMapping("/productList.php")
	public String productList(Model model) {
		model.addAttribute("products",repository.findAll());
		
		return "producttemplate"; //.html
	}

}
