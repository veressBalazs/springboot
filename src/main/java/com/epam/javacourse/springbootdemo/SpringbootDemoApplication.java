package com.epam.javacourse.springbootdemo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.epam.javacourse.springbootdemo.model.Product;
import com.epam.javacourse.springbootdemo.repository.ProductRepository;


@SpringBootApplication
public class SpringbootDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDemoApplication.class, args);
	}
	
	@Bean
	CommandLineRunner productFiller(ProductRepository repo) {
		return new CommandLineRunner() {
			
			@Override
			public void run(String... args) throws Exception {
				repo.save(new Product("Vasaló"));
				repo.save(new Product("Szárító"));
				repo.save(new Product("Mosógép"));
				repo.save(new Product("Vasaló"));
				
			}
		};
		
	}
}
