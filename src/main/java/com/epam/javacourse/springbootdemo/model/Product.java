package com.epam.javacourse.springbootdemo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Product  {
	
	@Id
	@GeneratedValue
	Long id;
	
	private String productName;

	public Product(String productName) {
		
		this.productName = productName;
	}

	public Product() {
		
	}

	public String getProductName() {
		return productName;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", productName=" + productName + "]";
	}
	
	
	
	

}
